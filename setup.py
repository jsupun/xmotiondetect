#!/usr/bin/env python

import epoch_version
from setuptools import setup, find_packages
from setuptools.command.install import install as install_command
import subprocess
import re


with open('requirements.txt') as f:
    required = []
    prog = re.compile("^[^-]")
    for row in f.read().splitlines():
        if prog.match(row) is None:
            continue
        required.append(row)


class Install(install_command):
    """ Customized setuptools install command which uses pip. """

    def run(self):
        subprocess.call(['pip', 'install', '-r', 'requirements.txt'])
        install_command.run(self)


setup(
    version=epoch_version.version(),
    name='xmotiondetect',
    description='Common logging',
    author='John Walstra',
    author_email='jkw2395@gmail.com',
    url='https://gitlab.com/jsupun/xlog',
    packages=find_packages(),
    package_data={},
    install_requires=required,
    include_package_data=True,
    cmdclass={
        'install': Install,
    },
)
