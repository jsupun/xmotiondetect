from .check_process_thread import CheckProcessThread
from .full_process_thread import FullProcessThread
from .app_log_watch import LogWatch
from .app_check_process import CheckProcess
from .app_full_process import FullProcess
from xconfig.schema import Schema


def __config_schema():
    return Schema.load_json_schema("config.yml")
