from xqueue import Queue
from xconfig import Config
from xlog import Log
import os


class LogWatch:

    def __init__(self):

        self.log_queue_name = Config.get("xmotiondetect.log.queue_name", "camera_ftp_log")
        self.check_queue_name = Config.get("xmotiondetect.video.check_queue_name", "check_video")

        self.queue = Queue(profile='default')
        self.queue.connect()
        self.queue.auto_setup()
        self.is_running = True

    @staticmethod
    def debug(msg, level=1, color=None):
        Log.debug(
            "Log Watcher Thread: {}".format(msg),
            level=level,
            color=color
        )

    def run(self):
        """
        Read the log from the message queue. Queue the MP4 files.
        :return:
        """

        def _on_receive(response):

            self.debug("Got message: {}".format(response), color=Log.cyan, level=1)

            message = response.message

            # 226 is a success status
            if message.get("status") == "226":

                date = message.get("date")
                file = message.get("path")
                num_bytes = message.get("bytes")

                filename, file_extension = os.path.splitext(file)
                if file_extension == ".mp4":

                    path_replace = Config.get("xmotiondetect.video.path_replace")
                    if path_replace is not None:
                        file = file.replace(path_replace.get("from"), path_replace.get("to"))
                        self.debug("Change path to {}".format(file))

                    self.queue.send(
                        queue=self.check_queue_name,
                        message={
                            "date": date,
                            "mp4_file": file,
                            "mp4_size": num_bytes
                        }
                    )

                self.queue.acknowledge(response)

        while self.is_running is True:
            self.debug("Waiting for message", color=Log.cyan)
            self.queue.receive(
                callback=_on_receive,
                queue=self.log_queue_name
            )

if __name__ == '__main__':
    Config()
    Log.init()
    LogWatch().run()
