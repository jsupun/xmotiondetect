from xconfig import Config
from xlog import Log
from xmotiondetect import FullProcessThread
import time
import signal


class FullProcess:

    def __init__(self):
        self.threads = []
        self.is_running = True
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def run(self):

        for thread_id in range(0, Config.get("xmotiondetect.full_process_threads", 1)):
            thread = FullProcessThread(id=thread_id)
            self.threads.append(thread)
            thread.start()

        while self.is_running is True:
            time.sleep(1)

    def shutdown(self):
        for thread in self.threads:
            thread.shutdown()
        self.is_running = False

    def exit_gracefully(self, signum, frame):
        self.shutdown()
        Log.shutdown()

if __name__ == '__main__':
    Config()
    Log.init(use_worker=True)
    FullProcess().run()
