from xdatabase import Deploy, Database, DefaultData
from xconfig import Config
from xlog import Log
import xmotiondetect.db as db


if __name__ == "__main__":
    Config()
    Log.init()
    Log.verbosity_level(10)

    deploy = Deploy()
    deploy.run(drop_existing=True)

    txn = Database.session()

    default_data = DefaultData()
    default_data.load()
