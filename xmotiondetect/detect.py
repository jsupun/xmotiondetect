from xlog import Log
import cv2
import tensorflow as tf
import numpy as np
from PIL import Image
import os


from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util


class Detect:

    """
    object_detection comes from https://github.com/tensorflow/models.git

    # cd models/research/
    # protoc object_detection/protos/*.proto --python_out=.

    Make sure models/research/ and models/research/slim is the PYTHONPATH

    """

    def __init__(self, id):
        self.id = id
        self.detection_graph = tf.Graph()

        models_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "models")
        gfile_path = os.path.join(models_dir, "frozen_inference_graph.pb")
        labelmap_path = os.path.join(models_dir, "mscoco_label_map.pbtxt")

        Log.debug("Model Dir is at {}".format(models_dir), level=2)
        Log.debug("Gfile Path is at {}".format(gfile_path), level=2)
        Log.debug("LabelMap Path is at {}".format(labelmap_path), level=2)

        with self.detection_graph.as_default():
            graph_def = tf.compat.v1.GraphDef()
            with tf.io.gfile.GFile(gfile_path, 'rb') as fid:
                serialized_graph = fid.read()
                graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(graph_def, name='')
        label_map = label_map_util.load_labelmap(labelmap_path)
        categories = label_map_util.convert_label_map_to_categories(
            label_map, max_num_classes=90, use_display_name=True)
        self.category_index = label_map_util.create_category_index(categories)

    def debug(self, msg, level=0, color=None):
        Log.debug("{}: {}".format(self.id, msg), level=level, color=color)

    @staticmethod
    def load_image_into_numpy_array(image):
        (im_width, im_height) = image.size
        return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)).astype(np.uint8)

    @staticmethod
    def get_show_category_id(category_index, category_name):

        for key in category_index.keys():
            if category_index[key].get("name") == category_name:
                return key
        return None

    def _detect(self, image_getter, image_writer, show_category_ids, frame_skip=0):

        category_detect = {}

        frames = 0
        with self.detection_graph.as_default():
            with tf.compat.v1.Session(graph=self.detection_graph) as sess:

                while True:

                    image_np = image_getter(frame_skip)
                    if image_np is None:
                        break

                    frames += 1

                    image_np_expanded = np.expand_dims(image_np, axis=0)
                    # Extract image tensor
                    image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
                    # Extract detection boxes
                    boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
                    # Extract detection scores
                    scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
                    # Extract detection classes
                    classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
                    # Extract number of detections
                    num_detections = self.detection_graph.get_tensor_by_name(
                        'num_detections:0')

                    # Actual detection.
                    (boxes, scores, classes, num_detections) = sess.run(
                        [boxes, scores, classes, num_detections], feed_dict={image_tensor: image_np_expanded})

                    s_boxes = np.squeeze(boxes)
                    s_scores = np.squeeze(scores)
                    s_classes = np.squeeze(classes).astype(np.int32)

                    seen_this_frame = {}
                    for index in list(range(int(num_detections[0]))):
                        category_id = int(s_classes.item(index))
                        if s_scores.item(index) > 0.0 and category_id not in show_category_ids:
                            s_scores.itemset(index, 0.0)
                        elif category_id not in seen_this_frame:
                            if category_id not in category_detect:
                                category_detect[category_id] = 0
                            category_detect[category_id] += 1
                            seen_this_frame[category_id] = True

                    # Visualization of the results of a detection.
                    vis_util.visualize_boxes_and_labels_on_image_array(
                        image_np,
                        s_boxes,
                        s_classes,
                        s_scores,
                        self.category_index,
                        use_normalized_coordinates=True,
                        line_thickness=8)

                    image_writer(image_np)

        stats = {}
        if frames > 0:
            for category_id in category_detect:
                stats[category_id] = category_detect[category_id] / frames

        return stats

    @staticmethod
    def get_category_ids(category_index, show_category_names):

        show_category_ids = []
        if show_category_names is not None:
            show_category_ids = [Detect.get_show_category_id(category_index, x) for x in show_category_names]

        return show_category_ids

    def picture(self, in_file, out_file, show_category_names=None):

        image = Image.open(in_file)
        image_np = Detect.load_image_into_numpy_array(image)
        show_category_ids = Detect.get_category_ids(self.category_index, show_category_names)
        image_array = [image_np]

        def image_getter(frame_skip):
            if len(image_array) > 0:
                return image_array.pop()
            return None

        def image_writer(save_image):
            cv2.imwrite(out_file, save_image)

        return self._detect(
            image_getter=image_getter,
            image_writer=image_writer,
            show_category_ids=show_category_ids,
        )

    def video(self, in_file, out_file, show_category_names=None, check_ratio=0.0):

        self.debug("Loading video file {}".format(in_file), level=2)
        self.debug("Saving to video file {}".format(out_file), level=2)

        cap = cv2.VideoCapture(in_file)
        if cap.isOpened() is False:
            Log.error("{}: Could not open video {}".format(self.id, in_file))
            return

        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        framerate = int(cap.get(cv2.CAP_PROP_FPS))
        frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self.debug("  Width  = {}".format(width), level=2)
        self.debug("  Height = {}".format(height), level=2)
        self.debug("  FPS    = {}".format(framerate), level=2)
        self.debug("  Frames = {}".format(frames), level=2)

        show_category_ids = self.get_category_ids(self.category_index, show_category_names)

        def image_getter(frame_skip_val=0):

            # Skip a specific # of frames
            if frame_skip_val > 0:
                for index in range(frame_skip_val):
                    cap.read()

            ret, image_np = cap.read()
            if ret == 0:
                return None
            return image_np

        codec = cv2.VideoWriter_fourcc(*'mp4v')
        resolution = (width, height)
        try:
            video_out = cv2.VideoWriter(
                out_file,
                apiPreference=cv2.CAP_ANY,
                fourcc=codec,
                fps=framerate,
                frameSize=resolution)
        except Exception as err:
            Log.error(err)
            raise

        def image_writer(image_np):
            video_out.write(image_np)

        frame_skip = 0
        if check_ratio > 0:
            frame_skip = int(frames * check_ratio)

        stats = self._detect(
            image_getter=image_getter,
            image_writer=image_writer,
            show_category_ids=show_category_ids,
            frame_skip=frame_skip
        )

        cap.release()

        return stats
