from xqueue import Queue, ConnectionLostException
from xconfig import Config
from xlog import Log
from xdatabase import Database
from .detect import Detect
import threading
import uuid
import re

import xmotiondetect.db as db


class FullProcessThread(threading.Thread):

    def __init__(self, id):

        threading.Thread.__init__(self)

        self.id = id

        try:
            self.detect = Detect(id="Full Process Thread {}".format(self.id))
        except Exception as err:
            Log.error("Detect fail: {}".format(err))
            raise

        try:
            self.video_queue = Queue(profile='default')
            self.video_queue.connect()
            self.video_queue.setup(
                queue_name=Config.get("xmotiondetect.video.full_queue_name", "full_video"),
                durable=True,
                auto_delete=False,
            )
        except Exception as err:
            Log.error("Video Queue fail: {}".format(err))
            raise

        self.alert_queue = None
        if Config.get("xmotiondetect.alert.enabled", False) is True:
            self.alert_queue = Queue(profile='alert')
            self.alert_queue.connect()

        txn = Database.session()

        try:
            self.locations = {}
            for location in txn.query(db.Location).all():
                self.locations[location.keyword] = {
                    "location_id": location.location_id,
                    "name": location.name
                }
        except Exception as err:
            Log.error("Location query fail: {}".format(err))
            raise

        try:
            self.categories = {}
            for category in txn.query(db.Category).all():
                self.categories[category.category_id] = {
                    "name": category.name,
                    "display_name": category.display_name
                }
        except Exception as err:
            Log.error("Category query fail: {}".format(err))
            raise

        self.alert_levels = {}
        if self.alert_queue is not None:
            for category in Config.get("xmotiondetect.alert.categories", []):
                category_id = 0
                for id in self.categories:
                    if self.categories[id]["name"] == category.get("name"):
                        category_id = id
                        break
                if category_id == 0:
                    Log.error("Cannot find category id for {}".format(category.get("name", "Unknown")))
                self.alert_levels[category_id] = category["min_perc"]

        self.is_running = True

    def shutdown(self):
        self.is_running = False
        self.video_queue.disconnect()

    def run(self):
        """
        This thread will check i check_process_thread.pyf the MP4 has the objects we are looking for and then
        sends it to be fulling processed.
        :return:
        """

        Log.debug("starting: {} id".format(self.id), level=1, color=Log.green)

        def _on_receive(response):

            message = response.message

            date = message.get("date")
            mp4_file = message.get("mp4_file")

            Log.debug("Got video to full process: {}".format(mp4_file), level=5)

            # We don't care about output, use a temp file
            out_file = "{}/{}.mp4".format(Config.get("xmotiondetect.video.output_dir", "/tmp"), str(uuid.uuid4()))

            Log.debug("  * write tp file {}".format(out_file), level=5)

            location_id = None
            location_name = None
            for keyword in self.locations:
                if re.search(keyword, mp4_file) is not None:
                    location_id = self.locations[keyword]["location_id"]
                    location_name = self.locations[keyword]["name"]

            # Unknown
            if location_id is None:
                # self.video_queue.acknowledge(response)
                Log.error("Cannot find location for MP4 file: {}".format(mp4_file))
                return

            stats = self.detect.video(
                in_file=mp4_file,
                out_file=out_file,
                show_category_names=Config.get("xmotiondetect.check_list", ['person']),
            )
            if stats is None:
                return

            num_stats = len(stats)
            if num_stats == 0:
                return

            txn = Database.session()
            video = db.Video(
                filename=out_file,
                ts=date,
                location_id=location_id
            )
            txn.add(video)
            for category_id in stats:
                db.VideoCategory(
                    video_id=video.video_id,
                    category_id=category_id,
                    detect_perc=stats[category_id]
                )

            txn.commit()

            if self.alert_queue is not None:
                alerts = []
                for category_id in stats:
                    if stats[category_id] >= self.alert_levels[category_id]:
                        name = self.categories[category_id]["display_name"]
                        alerts.append(
                            "{} at {:d}%".format(name, int(stats[category_id] * 100))
                        )

                if len(alerts) > 0:
                    self.alert_queue.send(
                        message={
                            location_name: {
                                "msg": "{} detected {}".format(location_name, ", ".join(alerts))
                            }
                        }
                    )

            self.video_queue.acknowledge(response)

        while self.is_running is True:
            Log.debug("Waiting for message", color=Log.cyan, level=1)
            try:
                self.video_queue.receive(
                    callback=_on_receive,
                    queue=Config.get("xmotiondetect.video.full_queue_name", "full_video")
                )
            except ConnectionLostException:
                pass
            except Exception as err:
                Log.exception(err)
