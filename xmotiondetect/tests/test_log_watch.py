#!/usr/bin/env python

import os
import unittest

from xlog import Log
from xconfig import Config
from xqueue import Test, Response
from xmotiondetect import LogWatch
import json


class TestLogWatch(unittest.TestCase):
    """Main smoke test"""

    def setUp(self):
        """ Setup the unit test """
        os.environ['APP_DIR'] = os.path.dirname(os.path.realpath(__file__))
        Config(settings={
            "xmotiondetect": {
                "log": {
                    "queue_name": "camera_ftp_log"
                },
                "video": {
                    "check_queue_name": "check_video",
                    "path_replace": {
                        "from": "/from",
                        "to": "/to"
                    }
                }
            },
            "xqueue": {
                "profiles": {
                    "default": {
                        "driver": "test",
                        "settings": {}
                    }
                }
            }
        })
        Log.init()
        Log.verbosity_level(10)

        Test.clear()


    def tearDown(self):
        """ Tear down the unit test and clear config settings so they reload. """
        Config.clear_settings()
        Test.clear()

    def test_log_watch_mp4(self):
        """ Test handling of the MP4"""

        Test.add_response(
            Response(
                obj={},
                message=json.dumps({
                    "line": "Not Important",
                    "date": "2020-08-25T02:53:17",
                    "ip": "10.0.1.67",
                    "action": "[]created",
                    "path": "/from/driveway/2020/08/24/Driveway_01_20200824215314.mp4",
                    "status": "226",
                    "user": "camera",
                    "bytes": "355281"
                }),
                content_type="application/json",
                topic="ftp.log"
            )
        )

        lw = LogWatch()

        def empty_responses_callback():
            lw.is_running = False
            return []

        Test.empty_responses_callback = empty_responses_callback
        lw.run()

        self.assertEqual(1, len(Test.published), "did not find 1 published message")
        response = Test.published[0]
        print(response)
        self.assertEqual(Config.get("xmotiondetect.video.check_queue_name"), response.get("queue"),
                         "check video queue is not correct")
        message = response.get("message")
        self.assertEqual("2020-08-25T02:53:17",message.get("date"), "date is correct" )
        self.assertEqual("355281", message.get("mp4_size"), "size is correct")
        self.assertEqual("/to/driveway/2020/08/24/Driveway_01_20200824215314.mp4", message.get("mp4_file"),
                         "path is correct")

    def test_log_watch_jpg(self):
        """ Test handling of the JPG"""

        Test.add_response(
            Response(
                obj={},
                message=json.dumps({
                    "line": "Not Important",
                    "date": "2020-08-25T02:53:17",
                    "ip": "10.0.1.67",
                    "action": "[]created",
                    "path": "/from/driveway/2020/08/24/Driveway_01_20200824215314.jpg",
                    "status": "226",
                    "user": "camera",
                    "bytes": "355281"
                }),
                content_type="application/json",
                topic="ftp.log"
            )
        )

        lw = LogWatch()

        def empty_responses_callback():
            lw.is_running = False
            return []

        Test.empty_responses_callback = empty_responses_callback
        lw.run()

        self.assertEqual(0, len(Test.published), "did not find 0 published message")


if __name__ == '__main__':
    unittest.main()
