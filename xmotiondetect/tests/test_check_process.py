#!/usr/bin/env python

import os
import unittest

from xlog import Log
from xconfig import Config
from xqueue import Test, Response
from xmotiondetect import CheckProcess
import json


class TestCheckProcess(unittest.TestCase):
    """Main smoke test"""

    def setUp(self):
        """ Setup the unit test """

        os.environ['APP_DIR'] = os.path.dirname(os.path.realpath(__file__))
        Config(settings={
            "xmotiondetect": {
                "video": {
                    "check_queue_name": "check_video",
                    "full_queue_name": "full_video",
                },
                "check_list": ["person", "car"],
                "check_ratio": 0.10
            },
            "xqueue": {
                "profiles": {
                    "default": {
                        "driver": "test",
                        "settings": {}
                    }
                }
            }
        })
        Log.init()
        Log.verbosity_level(10)

        self.mp4_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "movie", "Driveway_test.mp4")

        Test.clear()

    def tearDown(self):
        """ Tear down the unit test and clear config settings so they reload. """
        Config.clear_settings()
        Test.clear()

    def test_check_process(self):
        """ Test checking the video"""

        Test.add_response(
            Response(
                obj={},
                message=json.dumps({
                    "date": "2020-08-25T02:53:17",
                    "mp4_size": "355281",
                    "mp4_file": self.mp4_file
                }),
                content_type="application/json",
                topic="check_video"
            )
        )

        cp = CheckProcess()

        def empty_responses_callback():
            cp.exit_gracefully(15, None)
            return []

        Test.empty_responses_callback = empty_responses_callback
        cp.run()

        self.assertEqual(1, len(Test.published), "did not find 1 published message")
        response = Test.published[0]
        print(response)
        self.assertEqual(Config.get("xmotiondetect.video.full_queue_name"), response.get("queue"),
                         "full video queue is not correct")
        message = response.get("message")
        self.assertEqual("2020-08-25T02:53:17", message.get("date"), "date is correct")
        self.assertEqual(self.mp4_file, message.get("mp4_file"), "path is correct")


if __name__ == '__main__':
    unittest.main()
