#!/usr/bin/env python

import os
import unittest

from xlog import Log
from xconfig import Config
from xqueue import Test, Response
from xdatabase import Database, Deploy, DefaultData
from xmotiondetect import FullProcess
from tempfile import TemporaryDirectory
import json


class TestCheckProcess(unittest.TestCase):
    """Main smoke test"""

    def setUp(self):
        """ Setup the unit test """

        os.environ['APP_DIR'] = os.path.dirname(os.path.realpath(__file__))
        Config(settings={
            "xmotiondetect": {
                "video": {
                    "full_queue_name": "full_video",
                    "bytes_min": 100,
                    "output_dir": TemporaryDirectory().name
                },
                "check_list": ["person", "car"],
                "check_ratio": 0.10,
                "alert": {
                    "enabled": True,
                    "queuen_name": "ha/xmotiondetect",
                    "categories": [
                        {
                            "name": "person",
                            "min_perc": 0.0
                        },
                        {
                            "name": "car",
                            "min_perc": 0.5
                        }
                    ]
                }
            },
            "xqueue": {
                "profiles": {
                    "default": {
                        "driver": "test",
                        "settings": {}
                    },
                    "alert": {
                        "driver": "test",
                        "settings": {}
                    }
                }
            },
            "xdatabase": {
                "profiles": {
                    "default": {
                        "engine": "postgresql",
                        "host": os.environ.get("PGHOST"),
                        "port": os.environ.get("PGPORT"),
                        "user": os.environ.get("PGUSER"),
                        "password": os.environ.get("PGPASSWORD"),
                        "database": Database.test_database("hd_bulk"),
                        "models": ["xmotiondetect.db"]
                    },
                    "admin": {
                        "inherit": "default",
                        "database": "template1"
                    }
                }
            }
        })
        Log.init()
        Log.verbosity_level(10)
        Deploy().run(drop_existing=True)
        DefaultData().load()

        self.mp4_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "movie", "Driveway_test.mp4")

        Test.clear()

    def tearDown(self):
        """ Tear down the unit test and clear config settings so they reload. """
        Deploy().drop_database()
        Config.clear_settings()
        Test.clear()

    def test_check_process(self):
        """ Test checking the video"""

        Test.add_response(
            Response(
                obj={},
                message=json.dumps({
                    "date": "2020-08-25T02:53:17",
                    "mp4_file": self.mp4_file
                }),
                content_type="application/json",
                topic="full_video"
            )
        )

        fp = FullProcess()

        def empty_responses_callback():
            fp.exit_gracefully(15, None)
            return []

        Test.empty_responses_callback = empty_responses_callback
        fp.run()


if __name__ == '__main__':
    unittest.main()
