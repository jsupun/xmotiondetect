from sqlalchemy import Column, String, Integer
from xdatabase import Database


class Location(Database.Base):
    __tablename__ = 'location'

    location_id = Column(Integer, nullable=False, primary_key=True)
    name = Column(String, nullable=False)
    keyword = Column(String, nullable=False)

    def __init__(self, location_id, name, keyword):
        self.location_id = location_id
        self.name = name
        self.keyword = keyword
