from sqlalchemy import Column, String, Integer
from xdatabase import Database


class Category(Database.Base):
    __tablename__ = 'category'

    category_id = Column(Integer, nullable=False, primary_key=True)
    name = Column(String, nullable=False)
    display_name = Column(String, nullable=False)

    def __init__(self, category_id, name, display_name):
        self.category_id = category_id
        self.name = name
        self.display_name = display_name
