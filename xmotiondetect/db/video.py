from sqlalchemy import Column, String, DateTime, ForeignKey, Integer
from sqlalchemy.dialects.postgresql import UUID
from xdatabase import Database
import uuid


class Video(Database.Base):
    __tablename__ = 'video'

    video_id = Column(UUID(as_uuid=True), nullable=False, primary_key=True)
    filename = Column(String, nullable=False)
    location_id = Column(Integer, ForeignKey('location.location_id'), nullable=False)
    ts = Column(DateTime, nullable=False, index=True)

    def __init__(self, filename, ts, location_id):
        self.video_id = str(uuid.uuid4())
        self.filename = filename
        self.ts = ts
        self.location_id = location_id
