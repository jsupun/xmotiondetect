from sqlalchemy import Column, ForeignKey, Integer, Float
from sqlalchemy.dialects.postgresql import UUID
from xdatabase import Database


class VideoCategory(Database.Base):
    __tablename__ = 'video_category'

    video_id = Column(UUID(as_uuid=True), ForeignKey('video.video_id'), primary_key=True)
    category_id = Column(Integer, ForeignKey('category.category_id'), primary_key=True)
    detect_perc = Column(Float, default=0.0)

    def __init__(self, video_id, category_id, detect_perc=0.0 ):
        self.video_id = video_id
        self.category_id = category_id
        self.detect_perc = detect_perc
