from xqueue import Queue, ConnectionLostException
from xconfig import Config
from xlog import Log
from .detect import Detect
import tempfile
import threading
import os


class CheckProcessThread(threading.Thread):

    def __init__(self, id):

        threading.Thread.__init__(self)

        self.id = id

        try:
            self.detect = Detect(id="Check Process Thread")
        except Exception as err:
            Log.error("Detect fail: {}".format(err))
            raise

        self.queue = Queue(profile='default')
        self.queue.connect()
        self.queue.setup(
            queue_name=Config.get("xmotiondetect.video.check_queue_name", "check_video"),
            durable=True,
            auto_delete=False,
        )
        self.is_running = True

    def shutdown(self):
        self.is_running = False
        self.queue.disconnect()

    def run(self):
        """
        This thread will check i check_process_thread.pyf the MP4 has the objects we are looking for and then
        sends it to be fulling processed.
        :return:
        """

        Log.debug("Starting: {} id".format(self.id), level=1, color=Log.green)

        def _on_receive(response):
            message = response.message

            mp4_file = message.get("mp4_file")
            mp4_size = int(message.get("mp4_size"))
            date = message.get("date")

            if os.path.exists(mp4_file) is False:
                Log.error("The mp4 files does not exists at {}".format(mp4_file))
                return

            if mp4_size > Config.get("xmotiondetect.video.bytes_min", 0):
                # We don't care about output, use a temp file
                out_file = tempfile.NamedTemporaryFile(delete=True).name

                stats = self.detect.video(
                    in_file=mp4_file,
                    out_file=out_file,
                    show_category_names=Config.get("xmotiondetect.check_list", ['person']),
                    check_ratio=Config.get("xmotiondetect.check_ratio", 0.10)
                )

                if stats is not None and len(stats) > 0:
                    message = {
                        "date": date,
                        "mp4_file": mp4_file
                    }

                    self.queue.send(
                        queue=Config.get("xmotiondetect.video.full_queue_name", "full_video"),
                        message=message
                    )
                else:
                    Log.debug("Image did not have any of the categories,", level=2)
            else:
                Log.info("Image size too small {} <= {}".format(
                    mp4_size, Config.get("xmotiondetect.video.bytes_min", 0)
                ))

            self.queue.acknowledge(response)

        while self.is_running is True:
            Log.debug("Waiting for message", color=Log.cyan, level=1)

            try:
                self.queue.receive(
                    callback=_on_receive,
                    queue=Config.get("xmotiondetect.video.check_queue_name", "check_video")
                )
            except ConnectionLostException as err:
                Log.warning(err)
            except Exception as err:
                Log.exception(err)