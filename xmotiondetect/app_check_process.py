from xconfig import Config
from xlog import Log
from xmotiondetect import CheckProcessThread
import time
import signal


class CheckProcess:

    def __init__(self):
        self.threads = []
        self.is_running = True
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def run(self):

        Log.debug("Starting threads")

        num_threads = int(Config.get("xmotiondetect.check_process_threads", 1))
        if num_threads <= 0:
            raise Exception("Number of threads for check processor is not greater than 0")

        for thread_id in range(0, Config.get("xmotiondetect.check_process_threads", 1)):
            thread = CheckProcessThread(id=thread_id)
            self.threads.append(thread)
            thread.start()

        while self.is_running is True:
            time.sleep(1)

    def shutdown(self):
        for thread in self.threads:
            thread.shutdown()
        self.is_running = False

    def exit_gracefully(self, signum, frame):
        self.shutdown()
        Log.shutdown()

if __name__ == '__main__':
    Config()
    Log.init(use_worker=True)
    CheckProcess().run()
