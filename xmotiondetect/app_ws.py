from flask import Flask, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from xdatabase import Common, Database
from flask_restful import Api
from xconfig import Config
from xlog import Log
from xmotiondetect.controller import init as controller_init

workers_started = False

def create_app():
    app_uuid = "4686b5dc-cfa3-11ea-a23d-27d1946fc3a8"
    api = Api()
    app = Flask(__name__, static_url_path='')
    app.secret_key = app_uuid

    app.config['SQLALCHEMY_DATABASE_URI'] = Common().engine_url()
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    Database(override_session=SQLAlchemy(app).session)

    controller_init(api)
    # api.init_app(app)
    # ControllerBase.init(api)

    @app.route('/static/<path:path>')
    def send_js(path):
        return send_from_directory('static', path)

    return app


if __name__ == '__main__':
    Config()
    Log.init()

    my_app = create_app()
    my_app.run(debug=True, host='0.0.0.0')

